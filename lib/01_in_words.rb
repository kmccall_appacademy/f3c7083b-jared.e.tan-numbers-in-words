class Fixnum
  def in_words
    return 'zero' if self == 0
    nums = self.to_s.chars.map { |i| i.to_i }
    final_arr = []
#ones
    o = nums.last(3).join.to_i
    3.times { nums.pop }
    one = o.word
    final_arr << one unless o == 0
    return final_arr.reverse.join(' ') if nums.empty?
#thousands
    t = nums.last(3).join.to_i
    3.times { nums.pop }
    thousand = t.word + ' thousand' unless t == 0
    final_arr << thousand unless t == 0
    return final_arr.reverse.join(' ') if nums.empty?
#millions
    m = nums.last(3).join.to_i
    3.times { nums.pop }
    million = m.word + ' million' unless m == 0
    final_arr << million unless m == 0
    return final_arr.reverse.join(' ') if nums.empty?
#billions
    b = nums.last(3).join.to_i
    3.times { nums.pop }
    billion = b.word + ' billion' unless b == 0
    final_arr << billion unless b == 0
    return final_arr.reverse.join(' ') if nums.empty?
#trillions
    trillion = nums.last(3).join.to_i
    final_arr << trillion.word + ' trillion'
    final_arr.reverse.join(' ')
  end

  def word
    if self >= 0 && self < 10
      self.one_nine
    elsif self >= 10 && self < 20
      self.ten_nineteen
    elsif self >= 20 && self < 100 && self % 10 == 0
      (self - self % 10).tens
    elsif self >= 20 && self < 100 && self % 10 != 0
      (self - self % 10).tens + ' ' + (self % 10).one_nine
    elsif self >= 100 && self < 1000
      if self % 100 == 0
        (self / 100).one_nine + ' hundred'
      elsif self % 100 < 10
        (self / 100).one_nine + ' hundred ' + (self % 100).one_nine
      elsif self % 100 >= 10 && self % 100 < 20
        (self / 100).one_nine + ' hundred ' + (self % 100).ten_nineteen
      elsif self % 100 > 10 && self % 10 == 0
        (self / 100).one_nine + ' hundred ' + ((self % 100) - (self % 10)).tens
      else
        (self / 100).one_nine + ' hundred ' + ((self % 100) - (self % 10)).tens + ' ' + (self % 10).one_nine
      end
    end
  end

  def one_nine
    return '' if self == 0
    return 'one' if self == 1
    return 'two' if self == 2
    return 'three' if self == 3
    return 'four' if self == 4
    return 'five' if self == 5
    return 'six' if self == 6
    return 'seven' if self == 7
    return 'eight' if self == 8
    return 'nine' if self == 9
    ''
  end

  def ten_nineteen
    return 'ten' if self == 10
    return 'eleven' if self == 11
    return 'twelve' if self == 12
    return 'thirteen' if self == 13
    return 'fourteen' if self == 14
    return 'fifteen' if self == 15
    return 'sixteen' if self == 16
    return 'seventeen' if self == 17
    return 'eighteen' if self == 18
    return 'nineteen' if self == 19
    ''
  end

  def tens
    return 'twenty' if self >= 20 && self < 30
    return 'thirty' if self >= 30 && self < 40
    return 'forty' if self >= 40 && self < 50
    return 'fifty' if self >= 50 && self < 60
    return 'sixty' if self >= 60 && self < 70
    return 'seventy' if self >= 70 && self < 80
    return 'eighty' if self >= 80 && self < 90
    return 'ninety' if self >= 90 && self < 100
    ''
  end
end
